Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: x11rb
Upstream-Contact: Uli Schlachter <psychon@znc.in>
Source: https://github.com/psychon/x11rb

Files: *
Copyright: 2019-2020 Uli Schlachter <psychon@znc.in>
License: Expat or Apache-2.0

Files:
 src/cursor/find_cursor.rs
 src/cursor/parse_cursor.rs
Copyright:
 2013 Michael Stapelberg
 2002 Keith Packard
License: Expat or Apache-2.0

Files: examples/hypnomoire.rs
Copyright:
 2001-2002 Bart Massey and Jamey Sharp
 2019-2020 Uli Schlachter <psychon@znc.in>
License: Expat or Apache-2.0

Files: examples/tutorial.rs
Copyright:
 2001-2014 Bart Massey, Jamey Sharp, and Josh Triplett
 2019-2020 Uli Schlachter <psychon@znc.in>
License: Expat or Apache-2.0

Files: debian/*
Copyright:
 2020 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2020 Andrej Shadura <andrewsh@debian.org>
License: Expat or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
